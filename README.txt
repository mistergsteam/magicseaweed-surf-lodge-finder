When I was creating my weather Android application coursework, the group and I ran into a few problems regarding API keys and the only way
around it was to manually change the API keys for both google maps and google places.

Google Maps API key:
This key can be found under the values section in "google_maps_api.xml".

Google Places API Key:
This key can be found in MapsActivity.java under the method "getURL", it is part of the string builder used to create the URL sent
to the Google Places API.