package com.example.gilly.magicseaweedprogrammingchallenge;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.gilly.magicseaweedprogrammingchallenge.Model.Opening_hours;
import com.example.gilly.magicseaweedprogrammingchallenge.Model.Results;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ListIterator;

import javax.xml.transform.Result;

public class LodgeListActivity extends AppCompatActivity implements View.OnClickListener {

    private ListView listViewLodges;
    private Spinner spinnerSort;
    private Button btnGoBack;
    ArrayList<Results> lodgesFound = new ArrayList<Results>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lodge_list);

        listViewLodges = findViewById(R.id.lodgesFoundList);
        spinnerSort = findViewById(R.id.spinnerSort);
        btnGoBack = findViewById(R.id.buttonGoBack);
        btnGoBack.setOnClickListener(this);

        final ArrayList<Results> lodgesFound = (ArrayList<Results>) getIntent().getSerializableExtra("ListOfLodges");

        spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //highest to lowest has been chosen
                //Sorts the list so it displays high to low
                if(position == 0)
                {
                    Collections.sort(lodgesFound, new Comparator<Results>() {
                        @Override
                        public int compare(Results o1, Results o2) {

                            //Checks to see if it returns null
                            //If so then replace it with "0" instead
                            if(o1.getRating() == null)
                            {
                                o1.setRating("0");
                            }

                            if(o2.getRating() == null)
                            {
                                o2.setRating("0");
                            }

                            return o2.getRating().compareToIgnoreCase(o1.getRating());
                        }
                    });

                    //Loop through array
                    for(int i = 0; i < lodgesFound.size(); i ++)
                    {
                        LodgeList adapter = new LodgeList(LodgeListActivity.this, lodgesFound);
                        listViewLodges.setAdapter(adapter);
                    }
                }

                //Lowest to highest has been chosen
                //Sorts the list so it displays low to high
                else if(position == 1)
                {
                    Collections.sort(lodgesFound, new Comparator<Results>() {
                        @Override
                        public int compare(Results o1, Results o2) {

                            //Checks to see if it returns null
                            //If so then replace it with "0" instead
                            if(o1.getRating() == null)
                            {
                                o1.setRating("0");
                            }

                            if(o2.getRating() == null)
                            {
                                o2.setRating("0");
                            }

                            return o1.getRating().compareToIgnoreCase(o2.getRating());
                        }
                    });

                    //Loop through array
                    for(int i = 0; i < lodgesFound.size(); i ++)
                    {
                        LodgeList adapter = new LodgeList(LodgeListActivity.this, lodgesFound);
                        listViewLodges.setAdapter(adapter);
                    }
                }

                //Availability has been chosen
                //Sorts the list so it displays the available lodges first
                else if(position == 2)
                {
                    Collections.sort(lodgesFound, new Comparator<Results>() {
                        @Override
                        public int compare(Results o1, Results o2) {

                            //Checks to see if it returns null
                            //If so then replace it with a new object
                            //That's open_now is set to "N/A" instead
                            if(o1.getOpening_hours() == null)
                            {
                                Opening_hours opening_hours = new Opening_hours();
                                opening_hours.setOpen_now("N/A");
                                o1.setOpening_hours(opening_hours);
                            }

                            if(o2.getOpening_hours() == null)
                            {
                                Opening_hours opening_hours = new Opening_hours();
                                opening_hours.setOpen_now("N/A");
                                o2.setOpening_hours(opening_hours);
                            }

                            return o2.getOpening_hours().getOpen_now().compareToIgnoreCase(o1.getOpening_hours().getOpen_now());
                        }
                    });

                    //Loop through array
                    for(int i = 0; i < lodgesFound.size(); i ++)
                    {
                        LodgeList adapter = new LodgeList(LodgeListActivity.this, lodgesFound);
                        listViewLodges.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch(id)
        {
            //Redirects the user back to the map
            case R.id.buttonGoBack:
                Intent intent = new Intent(LodgeListActivity.this, MapsActivity.class);
                startActivity(intent);
                break;
        }
    }
}
