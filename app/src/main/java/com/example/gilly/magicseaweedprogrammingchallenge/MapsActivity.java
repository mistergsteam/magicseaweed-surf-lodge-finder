package com.example.gilly.magicseaweedprogrammingchallenge;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.gilly.magicseaweedprogrammingchallenge.Model.MyPlaces;
import com.example.gilly.magicseaweedprogrammingchallenge.Model.Results;
import com.example.gilly.magicseaweedprogrammingchallenge.Remote.IGoogleAPIService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener ,
        View.OnClickListener{

    //Google map and API client
    private GoogleMap mMap;
    private GoogleApiClient mGoogleAPIClient;

    //Assign MapActivity to the String 'TAG'
    private static final String TAG = "MapActivity";

    //Declare variables used in gaining permissions from the user
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUESTED_CODE = 1234;

    //Zoom default settings
    private static final float DEFAULT_ZOOM = 14f;
    private FusedLocationProviderClient mfusedLocationProviderClient;

    //Boolean Permission Check
    private Boolean mLocationPermissionGranted = false;

    //Location Declarations
    private double latitude, longitude;
    private LocationRequest mLocationRequest;
    IGoogleAPIService mService;

    //ArrayList to Store all the locations
    ArrayList<Results> placesNearByFound = new ArrayList<>();

    //Widgets
    private Button btnViewLodges;
    private ProgressDialog progressDialog;
    private ImageView mGPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mGPS = (ImageView) findViewById(R.id.ic_gps);
        btnViewLodges = (Button) findViewById(R.id.btnViewLodges);
        btnViewLodges.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);

        //Ask for permissions
        getLocationPermissions();

        mService = Common.getGoogleAPIService();
    }

    //Runs when the map is ready to be used by the user
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        Log.d(TAG, "ON MAP READY CALLED");
        //Toast.makeText(this, "map is ready", Toast.LENGTH_LONG).show();
        mMap = googleMap;

        //If all permissions are allowed then call the initialization method
        if (mLocationPermissionGranted) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);

            //On map load display their current location
            getDeviceLocation();
            moveCamera(new LatLng(latitude,longitude),DEFAULT_ZOOM, "My Location");

            //User clicks button to redirect the map to the user's current GPS location
            mGPS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "Clicked GPS Icon");
                    getDeviceLocation();
                }
            });
        }

        //Allow user to click on map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(latLng.latitude, latLng.longitude)).title("New Location");

                //Display Dialog to show that the places are being fetched
                progressDialog.setMessage("Searching for nearby locations...");
                progressDialog.show();

                //Clear the markers on the map and add a new one
                mMap.clear();
                mMap.addMarker(marker);

                //Change the new values for lat long
                latitude = latLng.latitude;
                longitude = latLng.longitude;

                //Recall the API with new coords
                nearByPlace();

                //Add circle to marker
                drawCircle(mMap);
            }
        });
    }

    //Get the current location of the device
    private void getDeviceLocation()
    {
        Log.d(TAG, "getting the device's current location");
        mfusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try
        {
            if(mLocationPermissionGranted)
            {
                final Task location = mfusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful())
                        {
                            Log.d(TAG, "found Location!!");
                            Location currentLocation = (Location) task.getResult();
                            latitude = currentLocation.getLatitude();
                            longitude = currentLocation.getLongitude();

                            //Used for debugging - these are the coords for Newquay (given in the specification document)
                            //latitude = 50.4164582;
                            //longitude = -5.1002;

                            LatLng latLng = new LatLng(latitude, longitude);
                            //Clear markers before creating new
                            mMap.clear();
                            moveCamera(latLng, DEFAULT_ZOOM, "Current Location");
                            drawCircle(mMap);
                            nearByPlace();

                        }
                        else
                        {
                            Log.d(TAG, "current Location is null");
                            Toast.makeText(MapsActivity.this,"unable to get current location", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        }
        catch (SecurityException ex)
        {
            Log.d(TAG, "securityException : " + ex);
        }
    }

    //Moves the camera using the lat and long, also placing a marker
    private void moveCamera(LatLng latLng, float zoom, String title)
    {
        Log.d(TAG,"moving the camera to : LAT - " + latLng.latitude + " AND LONG - " + latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

            //Create marker after the camera has been moved
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mMap.addMarker(options);
    }

    //Initialising the map
    public void initMap()
    {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);
    }

    //Getting the local permissions from the user
    private void getLocationPermissions()
    {
        Log.d(TAG, "LOCATION PERMISSIONS CALLED");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if(ContextCompat.checkSelfPermission(this.getApplicationContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionGranted = true;
                initMap();
            }
            else
            {
                ActivityCompat.requestPermissions(this, permissions,LOCATION_PERMISSION_REQUESTED_CODE);
            }
        }
        else
        {
            ActivityCompat.requestPermissions(this, permissions,LOCATION_PERMISSION_REQUESTED_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "ON REQUEST PERMISSIONS RESULT CALLED");
        mLocationPermissionGranted = false;

        switch(requestCode)
        {
            case LOCATION_PERMISSION_REQUESTED_CODE:
            {
                if(grantResults.length > 0)
                { for(int i = 0; i < grantResults.length; i++){
                    if(grantResults[i] != PackageManager.PERMISSION_GRANTED)
                    {
                        mLocationPermissionGranted = false;
                        return;
                    }
                }
                    mLocationPermissionGranted = true;
                    //Initialise the map
                    initMap();
                    buildGoogleAPIClient();
                }
            }
        }
    }

    //Building the google API client
    public void buildGoogleAPIClient()
    {
        //Setup the API Client
        mGoogleAPIClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleAPIClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleAPIClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //Taking the information from API and placing it in a ArrayList
    public void nearByPlace()
    {
        //Set Text of button to 0
        btnViewLodges.setText("VIEW LODGES (0 FOUND)");

        String url = getUrl(latitude, longitude);

        mService.getNearByPlaces(url)
                .enqueue(new Callback<MyPlaces>() {
                    @Override
                    public void onResponse(Call<MyPlaces> call, Response<MyPlaces> response) {
                        if (response.isSuccessful())
                        {
                            //Clear the arraylist
                            placesNearByFound.clear();

                            //Loop through and add each place found from the API to the arraylist
                            //Change the number on the button each time a new item is added
                            for (int i = 0; i < response.body().getResults().length; i++)
                            {
                                Results googlePlace = response.body().getResults()[i];
                                placesNearByFound.add(googlePlace);
                                //It is (i+1) because i starts at 0
                                btnViewLodges.setText("View Lodges (" + (i+1) + " Found)");
                            }

                            progressDialog.hide();
                        }
                    }

                    @Override
                    public void onFailure(Call<MyPlaces> call, Throwable t) {
                        Log.d(TAG, "It failed to retrieve information from the API");
                    }
                });
    }

    //Constructing the URL that will be sent to the API to find out specific information
    private String getUrl(double latitude, double longitude) {
        //Builds the Api URL that will be used to find the information from google
        StringBuilder googlePlacesURL = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesURL.append("location=" + latitude + "," + longitude);
        googlePlacesURL.append("&radius=" + 1000);
        googlePlacesURL.append("&type=lodging");
        googlePlacesURL.append("&keyword=surf");
        googlePlacesURL.append("&key=AIzaSyCHRaxzZrZil_Xw1GIw0yDYbae7WajJxG8");
        Log.d("getUrl", googlePlacesURL.toString());
        return googlePlacesURL.toString();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch(id)
        {
            case(R.id.btnViewLodges):
                String btnCheck = btnViewLodges.getText().toString();
                //Checks to see if the lodges found is 0
                //If so then do nothing, this means they can view an empty list
                if(btnCheck == ("VIEW LODGES (0 FOUND)"))
                {

                }
                //If the list is 1 or more then redirect them to LodgeList activity
                //Send over the ArrayList in the intent so it can be used to display the lodges found
                else
                {
                    Intent intent = new Intent(MapsActivity.this, LodgeListActivity.class);
                    intent.putExtra("ListOfLodges", placesNearByFound);
                    startActivity(intent);
                }
                break;
        }
    }

    //Setup the properites of the circles that are placed around the markers
    //The circles are used to show the radius size on the map
    public void drawCircle(GoogleMap googleMap)
    {
        Circle circle;
        CircleOptions circleOptions = new CircleOptions();
        circleOptions.radius(1000);
        circleOptions.strokeWidth(3);
        circleOptions.fillColor(0x44ff0000);
        circleOptions.strokeColor(getResources().getColor(R.color.circleRed));

        LatLng latLng = new LatLng(latitude, longitude);
        circleOptions.center(latLng);

        circle = googleMap.addCircle(circleOptions);
    }
}
