package com.example.gilly.magicseaweedprogrammingchallenge;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.gilly.magicseaweedprogrammingchallenge.Model.Results;

import java.util.List;

public class LodgeList extends ArrayAdapter<Results> {

    private Activity context;
    private List<Results> lodgeList;

    public LodgeList(@NonNull Activity context, List<Results> lodgeList) {
        super(context, R.layout.lodge_list_layout, lodgeList);
        this.context = context;
        this.lodgeList = lodgeList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.lodge_list_layout, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.textViewName);
        TextView textViewOpen = (TextView) listViewItem.findViewById(R.id.textViewOpeningTimes);
        TextView textViewRating = (TextView) listViewItem.findViewById(R.id.textViewRating);

        Results lodge = lodgeList.get(position);
        textViewName.setText(lodge.getName());
        String openStatus;

        //Check if the API returns null for opening hours
        //if it does then change it to N/A
        if(lodge.getOpening_hours() == null)
        {
            openStatus = ("N/A");
        }
        else
        {
            openStatus = lodge.getOpening_hours().getOpen_now();
        }
        textViewOpen.setText("Open Now: " + openStatus);

        //Check if the API returns null for ratings
        //if it does then change it to N/A
        String rating = lodge.getRating();
        if(lodge.getRating() == null)
        {
            rating = "N/A";
        }
        textViewRating.setText("Rating: " + rating);

        return listViewItem;
    }
}
